from django.shortcuts import redirect, render
from .forms import PeopleForm, ActivitiesForm
from .models import People, Activities

# Create your views here.
def register(request):
    form = PeopleForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('displayActivities')
    context = {'page_title': 'Register', 'form':form}
    return render(request, 'registerPeople.html', context)

def display(request):
    people = People.objects.all()
    activities = Activities.objects.all()
    context = {'people': people, 'activities': activities, 'page_title' : "Activities"}
    return render(request, "displayActivities.html",context)

def registerActivites(request):
    form = ActivitiesForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        return redirect('displayActivities')
    context = {'page_title': 'Register', 'form':form}
    return render(request, 'registerActivity.html', context)
