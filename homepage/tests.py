from django.test import TestCase, Client
from django.urls import resolve
from . import models
from .models import People, Activities
from .views import register, display, registerActivites
import datetime


# Create your tests here.
class Story6Test(TestCase):
    # Setting up the database
    def setUp(self):
        self.activites = Activities.objects.create(name="Table Tennis", date=datetime.date(2020, 5, 6))
        People.objects.create(full_name="Adrian", activities_registered=self.activites)
        People.objects.create(full_name="Vina", activities_registered=self.activites)

    # Checking Database
    def test_check_amount_people_database(self):
        self.assertEqual(len(People.objects.all()), 2)

    def test_check_amount_acitivites_database(self):
        self.assertEqual(len(Activities.objects.all()), 1)

    # Checking URLS
    def test_register_url_exist(self):
        response = Client().get('/register/')
        self.assertEqual(response.status_code, 200)

    def test_display_url_exist(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_register_activities_url_exist(self):
        response = Client().get('/registerActivities/')
        self.assertEqual(response.status_code, 200)

    # Checking Views
    def test_display_views_exist(self):
        found = resolve('/')
        self.assertEqual(found.func, display)

    def test_register_views_exist(self):
        found = resolve('/register/')
        self.assertEqual(found.func, register)

    def test_register_views_exist(self):
        found = resolve('/registerActivities/')
        self.assertEqual(found.func, registerActivites)

    # Checking Templates
    def test_register_templates_exist(self):
        response = Client().get('/register/')
        self.assertTemplateUsed(response, 'registerPeople.html')

    def test_display_templates_exist(self):
        response = Client().get('')
        self.assertTemplateUsed(response, 'displayActivities.html')

    def test_register_activities_templates_exist(self):
        response = Client().get('/registerActivities/')
        self.assertTemplateUsed(response, 'registerActivity.html')

    # Checking if HTML displaying models
    def test_if_HTML_display_models(self):
        response = Client().get('')
        html_response = response.content.decode('utf8')
        self.assertIn("Adrian", html_response)

    # Checking Form Exist & works
    def test_saving_a_POST_activities_request(self):
        response = Client().post('/registerActivities/', data={'name': 'Party', 'date': '2020-10-31'})
        amount = models.Activities.objects.filter(name="Party").count()
        self.assertEqual(amount, 1)

    def test_saving_a_POST_request(self):
        activites2 = Activities.objects.get(pk=1).pk
        response = Client().post('/register/', data={'full_name': 'Khan', 'activities_registered': activites2})
        amount = models.People.objects.filter(full_name='Khan').count()
        self.assertEqual(amount, 1)
