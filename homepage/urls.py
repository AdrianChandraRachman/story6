from django.urls import path
from .views import register, display, registerActivites
#url for app
urlpatterns = [
    path('', display, name='displayActivities'),
    path('register/', register, name='registerPeople'),
    path('registerActivities/', registerActivites, name='registerActivities')
]
