from django.forms import ModelForm, TextInput, DateInput
from .models import People, Activities

# Form Class
class PeopleForm(ModelForm):
    class Meta:
        model = People
        fields = ['full_name', 'activities_registered']
        widgets = {
            'full_name': TextInput(attrs={'placeholder': 'Adrian', 'required': True}),
            }

class ActivitiesForm(ModelForm):
    class Meta:
        model = Activities
        fields = ['name','date']
        widgets = {
            'name': TextInput(attrs={'placeholder': 'Tennis Meja', 'required': True}),
            'date':DateInput(attrs={'placeholder': 'YYYY-MM-DD', 'required': True}),
        }
